var five = require("johnny-five");
var board = new five.Board({port: "COM3"});
var morse = require("./morse");

var DOT_LENGTH = 200;
var DASH_LENGTH = DOT_LENGTH * 3;
var PAUSE_BETWEEN_CHARS = DASH_LENGTH;
var PAUSE_BETWEEN_WORDS = DOT_LENGTH * 7;

var led;
var remaining;

var transmit = function (input) {
    remaining = input.toUpperCase();
    handleNextLetter();
}


function handleNextLetter() {
    if (remaining != undefined && remaining.length > 0) {
        var first = remaining.substr(0, 1);
        remaining = remaining.substr(1);
        if (first == ' ') {
            setTimeout(handleNextLetter, PAUSE_BETWEEN_WORDS);
        } else {
            var sign = morse[first];
            console.log(first + "(" + sign + ") : ");
            lightTransmit(sign);
        }
    }
}

function lightTransmit(signs) {
    if (signs == undefined || signs.length == 0) {
        console.log(" ");
        setTimeout(handleNextLetter, PAUSE_BETWEEN_CHARS);
    } else {
        var currentSign = signs.substr(0, 1);
        console.log(currentSign);
        var next = signs.substr(1);
        led.on();
        setTimeout(function () {
            led.off();
            setTimeout(function () {
                lightTransmit(next);
            }, 150);
        }, currentSign == "." ? DOT_LENGTH : DASH_LENGTH);
    }
}


board.on("ready", function () {
    console.log("Board is ready!");
    console.log("Morse: " + morse);
    led = new five.Led(11);
    led.stop().off();
    this.repl.inject({
        led: led,
        transmit: transmit
    });
});
