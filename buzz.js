var five = require("johnny-five");
var board = new five.Board({port: "COM3"});

board.on("ready", function () {
    console.log("Klar! Her kommer Lisa...");
    var piezo = new five.Piezo(3); //output går til Pin 3
    piezo.play({
        tempo: 100,
        song: [
            ["c5", 1], ["d5", 1], ["e5", 1], ["f5", 1],["g5", 1.75], [null, .25], ["g5", 2],
            ["a5", 1], ["a5", 1], ["a5", 1], ["a5", 1],["g5", 4],
            ["f5", 1], ["f5", 1], ["f5", 1], ["f5", 1],["e5", 1.75], [null, .25], ["e5", 2],
            ["d5", 1], ["d5", 1], ["d5", 1], ["d5", 1],["c5", 4]
        ]
    });
});