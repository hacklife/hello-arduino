var five = require("johnny-five");
var board = new five.Board({port: "COM3"});
var counter = 0;

board.on("ready", function() {

    // Create a new `button` hardware instance.
    var button = new five.Button(7);

    button.on("hold", function() {
        console.log( "Button held" );
    });

    button.on("press", function() {
        counter++;
        console.log( "Button pressed " +counter+ " times");
    });

    button.on("release", function() {
        console.log( "Button released" );
    });
});